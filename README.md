# WebSocket broadcaster

Useful for non blocking websocket listeners.

Spins up a go routine for each connected websocket, and uses a buffered
channel for each of them. When channel is full then the websocket is
closed.

