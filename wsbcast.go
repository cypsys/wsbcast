package wsbcast

import (
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
)

type Config struct {
	Upgrader  websocket.Upgrader
	OnConnect func(r *http.Request) <-chan []byte
	Incoming  func(b []byte) []byte
}

type WebsocketBroadcastHandler struct {
	config  Config
	mu      sync.Mutex
	clients []chan []byte
}

func (h *WebsocketBroadcastHandler) newClientCh(conn *websocket.Conn, r *http.Request) chan []byte {
	ch := make(chan []byte, 10)
	h.mu.Lock()
	h.clients = append(h.clients, ch)
	h.mu.Unlock()
	go func() {
		if h.config.OnConnect != nil {
			for data := range h.config.OnConnect(r) {
				conn.WriteMessage(websocket.TextMessage, data)
			}
		}
		for data := range ch {
			conn.WriteMessage(websocket.TextMessage, data)
		}
		conn.Close()
	}()
	return ch
}

func (h *WebsocketBroadcastHandler) closeClientCh(ch chan []byte) {
	h.mu.Lock()
	for i, key := range h.clients {
		if key == ch {
			close(ch)
			h.clients = append(h.clients[:i], h.clients[i+1:]...)
			break
		}
	}
	h.mu.Unlock()
}

func (h *WebsocketBroadcastHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := h.config.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer conn.Close()

	ch := h.newClientCh(conn, r)

	for {
		_, b, err := conn.ReadMessage()
		if err != nil {
			break
		}
		if h.config.Incoming != nil {
			b := h.config.Incoming(b)
			if b != nil {
				h.unicast(ch, b)
			}
		}
	}

	h.closeClientCh(ch)
}

func (h *WebsocketBroadcastHandler) unicast(ch chan []byte, data []byte) {
	h.mu.Lock()
	for i, cch := range h.clients {
		if ch == cch {
			select {
			case ch <- data:
			default: // Purge channel on overflow
				close(ch)
				h.clients = append(h.clients[:i], h.clients[i+1:]...)
			}
			break
		}
	}
	h.mu.Unlock()
}

func (h *WebsocketBroadcastHandler) Broadcast(data []byte) {
	h.mu.Lock()
	purge := []chan []byte{}
	for _, ch := range h.clients {
		select {
		case ch <- data:
		default: // Purge channel on overflow
			close(ch)
			purge = append(purge, ch)
		}
	}
	for _, pch := range purge {
		for i, ch := range h.clients {
			if pch == ch {
				h.clients = append(h.clients[:i], h.clients[i+1:]...)
				break
			}
		}
	}
	h.mu.Unlock()
}

func WebsocketBroadcaster(config Config) *WebsocketBroadcastHandler {
	wsb := &WebsocketBroadcastHandler{
		config: config,
	}
	return wsb
}
